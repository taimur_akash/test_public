package com.mindvally.image;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;

/**
 * Created by Taimur on 11/12/2016.
 */
public class ImageLoader extends AsyncTask<String, Integer, Bitmap> {

    private final WeakReference<ImageView> viewReference;

    public ImageLoader( ImageView view ) {
        viewReference = new WeakReference<ImageView>( view );
    }

    @Override
    protected Bitmap doInBackground( String... params ) {
        try {
            java.net.URL url = new java.net.URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }
    }


    @Override
    protected void onPostExecute( Bitmap bitmap ) {
        ImageView imageView = viewReference.get();
        if( imageView != null ) {
            imageView.setImageBitmap( bitmap );
        }
    }



}
