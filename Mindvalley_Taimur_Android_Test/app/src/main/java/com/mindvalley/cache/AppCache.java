package com.mindvalley.cache;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Taimur on 11/12/2016.
 */
public class AppCache  {
    private static AppCache instance;
    private LruCache<Object, Object> lru;

    private AppCache() {

        lru = new LruCache<Object, Object>(1024);

    }

    public static AppCache getInstance() {

        if (instance == null) {

            instance = new AppCache();
        }

        return instance;

    }

    public LruCache<Object, Object> getLru() {
        return lru;
    }
}
