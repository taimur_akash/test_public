package com.mindvalley.cache;

/**
 * Created by Taimur on 11/12/2016.
 */
public class CacheFactory {

    public Object getFactory(String objType){
        if(objType.equals("AppCache")){
            return AppCache.getInstance();
        }

        return null;
    }
}
