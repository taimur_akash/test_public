package com.mindvalley.main;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.mindvalley.cache.AppCache;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveBitmapToCahche(){

        //The imageView that you want to save it's bitmap image resourse
        ImageView imageView = (ImageView) findViewById(R.id.imageViewID);

        //To get the bitmap from the imageView
        Bitmap bitmap = ((BitmapDrawable)imageview.getDrawable()).getBitmap();

        //Saving bitmap to cache. it will later be retrieved using the bitmap_image key
        AppCache.getInstance().getLru().put("bitmap_image", bitmap);
    }

    public void retrieveBitmapFromCache(){

        //The imageView that you want to set to the retrieved bitmap
        ImageView imageView = (ImageView) findViewById(R.id.imageViewID);

        //To get bitmap from cache using the key. Must cast retrieved cache Object to Bitmap
        Bitmap bitmap = (Bitmap)AppCache.getInstance().getLru().get("bitmap_image");

        //Setting imageView to retrieved bitmap from cache
        imageView.setImageBitmap(bitmap);

    }
}
